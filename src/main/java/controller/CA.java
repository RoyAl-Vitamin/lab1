package controller;

import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.*;

public class CA implements Runnable {

    private static final Logger log = LogManager.getLogger(CA.class);

    private final Canvas canvas;

    private WrapperImageSet imageSet;

    private int iteration = 0;

    private final static double P_CREATE = 0.7; // Вероятность добавления машинки на поле

    private final static int SLEEP = 150;//750; // Время, на которое тормозим поток, обрабатывающий машинки

    private TypeCell[][] field;

    private ArrayList<Cell> gate;

    private ArrayList<Car> cars;

    private static int numCar = 10;

    private volatile boolean cont = true;

    public CA(Canvas valCanvas) {
        this.canvas = valCanvas;
    }

    public void setInit(TypeCell[][] valField) {
        this.field = valField;
        imageSet = WrapperImageSet.getInstance();
        cars = new ArrayList();
        setGate();
    }

    public void resume() {
        cont = true;
    }

    public void pause() {
        log.info("iteration == " + iteration);
        cont = false;
    }

    /**
     * Устанавливает свободные входы мз-за границы экрана
     */
    private void setGate() {
        gate = new ArrayList<>();
        for (int i = 0; i < field.length; i++) {
            if (field[i][0] == TypeCell.D) {
//                log.debug("Cell [" + i + "; " +  0 + "]");
                gate.add(new Cell(i, 0));
            }
            if (field[i][field[i].length - 1] == TypeCell.U) {
//                log.debug("Cell [" + i + "; " +  (field[i].length - 1) + "]");
                gate.add(new Cell(i, field[i].length - 1));
            }
        }
        for (int j = 0; j < field.length; j++) { // field[j].length
            if (field[0][j] == TypeCell.R) {
//                log.debug("Cell [" + 0 + "; " +  j + "]");
                gate.add(new Cell(0, j));
            }
            if (field[field.length - 1][j] == TypeCell.L) {
//                log.debug("Cell [" + (field.length - 1) + "; " +  j + "]");
                gate.add(new Cell(field.length - 1, j));
            }
        }
    }

    // Здесь присходит расчёт и отрисовка на canvas каждый i-ый шаг и пока не остановили (cont)
    @Override
    public void run() {
        Random r = new Random();
        try {
            while (cont) {
                if (cars.size() < numCar && r.nextDouble() > P_CREATE) { // Машинки добавляются постепенно
                    addCar();
                }

                for (Iterator<Car> iter = cars.iterator(); iter.hasNext();){ // Перестановка машинок, рандомно
                    Car curCar = iter.next();
                    switch (field[curCar.x][curCar.y]) {
                        case U: // Если машинка стоит на стрелке "вверх"
                            if (curCar.y > 0) {
                                if (isClear(curCar.x, curCar.y - 1)) {
                                    up(curCar);
                                }
                            } else {
                                removeCar(iter); // Убираем машинку
                            }
                            break;
                        case D: // Если машинка стоит на стрелке "вниз"
                            if (curCar.y < field[curCar.x].length - 1) {
                                if (isClear(curCar.x, curCar.y + 1)) {
                                    down(curCar);
                                }
                            } else {
                                removeCar(iter); // Убираем машинку
                            }
                            break;
                        case R: // Если машинка стоит на стрелке "вправо"
                            if (curCar.x < field.length - 1) {
                                if (isClear(curCar.x + 1, curCar.y)) {
                                    right(curCar);
                                }
                            } else {
                                removeCar(iter); // Убираем машинку
                            }
                            break;
                        case L: // Если машинка стоит на стрелке "влево"
                            if (curCar.x > 0) {
                                if (isClear(curCar.x - 1, curCar.y)) {
                                    left(curCar);
                                }
                            } else {
                                removeCar(iter); // Убираем машинку
                            }
                            break;
                        case UR: // Можем двигаться только вверх или вправо
                            if (curCar.x - curCar.x_prev != 0) { // заехали справа
                                if (isClear(curCar.x, curCar.y - 1)) {
                                    curCar.x_prev = curCar.x;
//                                    log.debug("заехали справа");
                                    up(curCar);
                                }
                            } else if (curCar.y - curCar.y_prev != 0) { // заехали сверху
                                if (isClear(curCar.x + 1, curCar.y)) {
                                    curCar.y_prev = curCar.y;
//                                    log.debug("заехали сверху");
                                    right(curCar);
                                }
                            }
                            break;
                        case DR: // Можем двигаться только вниз или вправо
                            if (curCar.x - curCar.x_prev != 0) { // заехали справа
                                if (isClear(curCar.x, curCar.y + 1)) {
                                    curCar.x_prev = curCar.x;
//                                    log.debug("заехали справа");
                                    down(curCar);
                                }
                            } else if (curCar.y - curCar.y_prev != 0) { // заехали снизу
                                if (isClear(curCar.x + 1, curCar.y)) {
                                    curCar.y_prev = curCar.y;
//                                    log.debug("заехали снизу");
                                    right(curCar);
                                }
                            }
                            break;
                        case DL: // Можем двигаться только вниз и влево
                            if (curCar.x - curCar.x_prev != 0) { // заехали слева
                                if (isClear(curCar.x, curCar.y + 1)) {
                                    curCar.x_prev = curCar.x;
//                                    log.debug("заехали слева");
                                    down(curCar);
                                }
                            } else if (curCar.y - curCar.y_prev != 0) { // заехали снизу
                                if (isClear(curCar.x - 1, curCar.y)) {
                                    curCar.y_prev = curCar.y;
//                                    log.debug("заехали снизу");
                                    left(curCar);
                                }
                            }
                            break;
                        case UL: // Можем двигаться только вверх и влево
                            if (curCar.x - curCar.x_prev != 0) { // заехали слева
                                if (isClear(curCar.x, curCar.y - 1)) {
                                    curCar.x_prev = curCar.x;
//                                    log.debug("заехали слева");
                                    up(curCar);
                                }
                            } else if (curCar.y - curCar.y_prev != 0) { // заехали сверху
                                if (isClear(curCar.x - 1, curCar.y)) {
                                    curCar.y_prev = curCar.y;
//                                    log.debug("заехали сверху");
                                    left(curCar);
                                }
                            }
                            break;
                        case URL: // Можем двигаться только вверх и влево, вправо
                            if (curCar.x - curCar.x_prev > 0) { // заехали слева
                                curCar.x_prev = curCar.x;
//                                log.debug("заехали слева");
                                onRandomWay(curCar, TypeCell.D, TypeCell.L);
                            } else if (curCar.x - curCar.x_prev < 0) { // заехали справа
                                curCar.x_prev = curCar.x;
//                                log.debug("заехали справа");
                                onRandomWay(curCar, TypeCell.D, TypeCell.R);
                            } else if (curCar.y - curCar.y_prev != 0) { // заехали сверху
                                curCar.y_prev = curCar.y;
//                                log.debug("заехали сверху");
                                onRandomWay(curCar, TypeCell.D, TypeCell.U);
                            }
                            break;
                        case UDR:
                            if (curCar.y - curCar.y_prev > 0) { // заехали сверху
                                curCar.y_prev = curCar.y;
//                                log.debug("заехали сверху");
                                onRandomWay(curCar, TypeCell.L, TypeCell.U);
                            } else if (curCar.y - curCar.y_prev < 0) { // заехали снизу
                                curCar.y_prev = curCar.y;
//                                log.debug("заехали снизу");
                                onRandomWay(curCar, TypeCell.L, TypeCell.D);
                            } else if (curCar.x - curCar.x_prev != 0) { // заехали справа
                                curCar.x_prev = curCar.x;
//                                log.debug("заехали справа");
                                onRandomWay(curCar, TypeCell.L, TypeCell.R);
                            }
                            break;
                        case DRL:
                            if (curCar.x - curCar.x_prev > 0) { // заехали слева
                                curCar.x_prev = curCar.x;
//                                log.debug("заехали слева");
                                onRandomWay(curCar, TypeCell.U, TypeCell.L);
                            } else if (curCar.x - curCar.x_prev < 0) { // заехали справа
                                curCar.x_prev = curCar.x;
//                                log.debug("заехали справа");
                                onRandomWay(curCar, TypeCell.U, TypeCell.R);
                            } else if (curCar.y - curCar.y_prev != 0) { // заехали снизу
                                curCar.y_prev = curCar.y;
//                                log.debug("заехали снизу");
                                onRandomWay(curCar, TypeCell.U, TypeCell.D);
                            }
                            break;
                        case UDL:
                            if (curCar.y - curCar.y_prev > 0) { // заехали сверху
                                curCar.y_prev = curCar.y;
//                                log.debug("заехали сверху");
                                onRandomWay(curCar, TypeCell.R, TypeCell.U);
                            } else if (curCar.y - curCar.y_prev < 0) { // заехали снизу
                                curCar.y_prev = curCar.y;
//                                log.debug("заехали снизу");
                                onRandomWay(curCar, TypeCell.R, TypeCell.D);
                            } else if (curCar.x - curCar.x_prev != 0) { // заехали слева
                                curCar.x_prev = curCar.x;
//                                log.debug("заехали слева");
                                onRandomWay(curCar, TypeCell.R, TypeCell.L);
                            }
                            break;
                        case UDRL:
                            if (curCar.y - curCar.y_prev > 0) { // заехали сверху
                                curCar.y_prev = curCar.y;
//                                log.debug("заехали сверху");
                                onRandomWay(curCar, TypeCell.U);
                            } else if (curCar.y - curCar.y_prev < 0) { // заехали снизу
                                curCar.y_prev = curCar.y;
//                                log.debug("заехали снизу");
                                onRandomWay(curCar, TypeCell.D);
                            } if (curCar.x - curCar.x_prev > 0) { // заехали слева
                                curCar.x_prev = curCar.x;
//                                log.debug("заехали слева");
                                onRandomWay(curCar, TypeCell.L);
                            } else if (curCar.x - curCar.x_prev < 0) { // заехали справа
                                curCar.x_prev = curCar.x;
//                                log.debug("заехали справа");
                                onRandomWay(curCar, TypeCell.R);
                            }
                            //break;
                        default: // WALL, NONE но такого быть не должно
                            break;
                    }
                    //log.debug("previuos: [" + curCar.x_prev + "][" + curCar.y_prev + "]");
                    //log.debug("current : [" + curCar.x + "][" + curCar.y + "]");
                    repaint();
                }
                iteration++;

                Thread.currentThread().sleep(SLEEP); // притормозим этот поток
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            log.error("Видимо, машинка не загрузилась");
            e.printStackTrace();
        }
    }

    private void up(Car car) {
        car.y_prev = car.y;
        car.y--;
        car.state = State.V;
//        log.info("Машинка поехала вверх! [" + car.x + "][" + car.y + "]");
    }

    private void down(Car car) {
        car.y_prev = car.y;
        car.y++;
        car.state = State.V;
//        log.info("Машинка поехала вниз! [" + car.x + "][" + car.y + "]");
    }

    private void right(Car car) {
        car.x_prev = car.x;
        car.x++;
        car.state = State.H;
//        log.info("Машинка поехала вправо! [" + car.x + "][" + car.y + "]");
    }

    private void left(Car car) {
        car.x_prev = car.x;
        car.x--;
        car.state = State.H;
//        log.info("Машинка поехала влево! [" + car.x + "][" + car.y + "]");
    }

    private void repaint() throws Exception {
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        for (Car car : cars) {
            Image image = imageSet.getByName("Car" + car.state.toString());
            if (image == null) {
                log.error("Не могу загрузить картинку машинки");
                throw new Exception("Не могу загрузить картинку машинки");
            }
            canvas.getGraphicsContext2D().drawImage(image,
                    car.x * imageSet.getSize() + 1,
                    car.y * imageSet.getSize() + 1,
                    imageSet.getSize() - 2,
                    imageSet.getSize() - 2);
        }
    }

    /**
     * если на эту клетку нельзя заезжать (WALL, NONE), то возварщается false
     * если какая-нибудь из машинок стоит на этой клетке, то возвращается false
     * @param x x-coordinate
     * @param y y-coordinate
     * @return true if cell is clear
     */
    private boolean isClear(int x, int y) {
        if (field[x][y] == TypeCell.WALL || field[x][y] == TypeCell.NONE) return false;
        for (Car car : cars) {
            if (car.x == x && car.y == y) return false;
        }
        return true;
    }

    /**
     * Указывает рандомное направление движения свободное от машин
     * @param typeCellsNotUse направления, куда нельзя двигаться
     */
    private void onRandomWay(Car car, TypeCell... typeCellsNotUse) {
        // Составляем список возможных направлений
        List<TypeCell> list = new ArrayList<>();
        list.addAll(Arrays.asList(TypeCell.fracturedWay(field[car.x][car.y])));

        // Удаляем лишние
        if (typeCellsNotUse != null && typeCellsNotUse.length != 0) {
            list.removeAll(Arrays.asList(typeCellsNotUse));
        }

        // и в те, которые зайти нельзя (из которых вышли)
        if (car.x - car.x_prev > 0) {
            list.remove(TypeCell.L);
        }
        if (car.x - car.x_prev < 0) {
            list.remove(TypeCell.R);
        }
        if (car.y - car.y_prev > 0) {
            list.remove(TypeCell.U);
        }
        if (car.y - car.y_prev < 0) {
            list.remove(TypeCell.D);
        }

        // Переходим по любому из них
        Random r = new Random();
        while (true) {
            switch (list.get(r.nextInt(list.size()))) {
                case U:
                    if (
                            field[car.x][car.y - 1] != TypeCell.D &&
                            field[car.x][car.y - 1] != TypeCell.WALL &&
                            isClear(car.x,car.y - 1)
                            ) {
                        up(car);
                        return;
                    }
                case D:
                    if (
                            field[car.x][car.y + 1] != TypeCell.U &&
                            field[car.x][car.y + 1] != TypeCell.WALL &&
                            isClear(car.x,car.y + 1)
                            ) {
                        down(car);
                        return;
                    }
                case R:
                    if (
                            field[car.x + 1][car.y] != TypeCell.L &&
                            field[car.x + 1][car.y] != TypeCell.WALL &&
                            isClear(car.x + 1,car.y)
                            ) {
                        right(car);
                        return;
                    }
                case L:
                    if (
                            field[car.x - 1][car.y] != TypeCell.R &&
                            field[car.x - 1][car.y] != TypeCell.WALL &&
                            isClear(car.x - 1,car.y)
                            ) {
                        left(car);
                        return;
                    }
            }
        }
    }

    /*
     * Добавляет машинку в массив и ставит на поле
     */
    private void addCar() throws Exception {
        Car car = new Car();
        Random r = new Random();
        if (gate.size() == 0) {
            log.debug("Количество въездов на поле == 0");
            return;
        }
        Cell c = gate.get(r.nextInt(gate.size()));
        car.x = c.x;
        car.y = c.y;
        car.x_prev = c.x;
        car.y_prev = c.y;
        switch (field[car.x][car.y]) {
            case U:
            case D:
                car.state = State.V;
                break;
            case L:
            case R:
                car.state = State.H;
                break;
            default:
                car.state = State.H;
        }
        cars.add(car);
        Image image = imageSet.getByName("Car" + car.state.toString());
        if (image == null) {
            log.error("Не могу загрузить картинку машинки");
            throw new Exception("Не могу загрузить картинку машинки");
        }
        canvas.getGraphicsContext2D().drawImage(image,
                car.x * imageSet.getSize() + 1,
                car.y * imageSet.getSize() + 1,
                imageSet.getSize() - 2,
                imageSet.getSize() - 2);
    }

    private void removeCar(Iterator<Car> iterCar) {
        iterCar.remove();
//        log.info("Машинка ушла с поля");
    }

    public static void setNumCar(int vnumCar) {
        numCar = vnumCar;
    }

    public static int getNumCar() {
        return numCar;
    }
}
