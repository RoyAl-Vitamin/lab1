package controller;

import java.util.Random;

public class Car {

    public int x, y; // координаты

    public int x_prev, y_prev; // предыдущие координаты

    public double p; // Вероятность поломки

    public State state;

    public Car() {
        p = ((new Random()).nextInt(10)) / 100;
        x_prev = y_prev = x = y = -1;
        state = State.H;
    }
}
