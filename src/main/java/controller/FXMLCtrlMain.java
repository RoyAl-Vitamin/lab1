package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class FXMLCtrlMain extends HBox {

    private static final Logger log = LogManager.getLogger(FXMLCtrlMain.class);

    private Stage stage;

    private FXMLCtrlMain controller;

    private Thread mainThread;

    private CA ca;

    private TypeCell cursorType;

    private TypeCell[][] field;

    private WrapperImageSet imageSet;

    @FXML
    private ToggleGroup toggleGroup;

    @FXML
    private Canvas canvas;

    @FXML
    private Canvas canvasCar;

    @FXML
    private Button calc;

    @FXML
    private Button stop;

    @FXML
    private ChoiceBox<String> variants;

    @FXML
    private TextField quantityCar;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setController(FXMLCtrlMain controller) {
        this.controller = controller;
    }

    public void init() {
        stop.setDisable(true);
        imageSet = WrapperImageSet.getInstance();
        imageSet.setSize(20);
        imageSet.reload();

        String[] tempLink = new String[imageSet.apply.size()];
        tempLink = imageSet.apply.toArray(tempLink);
        variants.setItems(FXCollections.observableArrayList(tempLink));
        variants.getSelectionModel().select("20");

        variants.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, //
                                String oldValue, String newValue) {
                if (newValue != null) {
                    if (ca != null) {
                        clickStop();
                    }
                    log.debug("new cell size == " + Integer.valueOf(newValue));
                    imageSet.setSize(Integer.valueOf(newValue));
                    imageSet.reload();
                    resize();

                }
            }
        });

        quantityCar.setText(String.valueOf(CA.getNumCar()));

        stage.setOnCloseRequest(e -> {
            if (mainThread != null && mainThread.isAlive()) {
                mainThread.stop();
                log.info("Расчётный поток завершён");
            }
        });

        cursorType = TypeCell.NONE;

        resize();

        canvas.addEventHandler(
                MouseEvent.MOUSE_CLICKED,
                event -> {
                    clickOnCanvasHandler(event);
                }
        );

        canvas.addEventHandler(
                MouseEvent.MOUSE_DRAGGED,
                event -> {
                    clickOnCanvasHandler(event);
                }
        );

        toggleGroup.selectedToggleProperty().addListener(event -> {
            if (toggleGroup.getSelectedToggle() != null) {
                cursorType = TypeCell.valueOf(((ToggleButton) toggleGroup.getSelectedToggle()).getId());
            } else {
                cursorType = TypeCell.NONE;
            }
        });
    }

    private void resize() {
        //canvas.getGraphicsContext2D().clearRect(0,0, widthProperty().doubleValue(), heightProperty().doubleValue());
        canvas.getGraphicsContext2D().fillRect(0,0, canvas.getWidth(), canvas.getHeight());
        /* Ставим пикчи вместо названий */
        for (Toggle tb : toggleGroup.getToggles()) {
            ImageView iv = new ImageView(imageSet.getByName(((ToggleButton) tb).getId()));
            iv.setFitHeight(20);
            iv.setFitWidth(20);
            ((ToggleButton) tb).setGraphic(iv);
        }

        /* Собираем поле */
        field = new TypeCell[(int) canvas.getWidth() / imageSet.getSize()][(int) canvas.getHeight() / imageSet.getSize()];
        for (int i = 0; i < field.length; i++)
            for (int j = 0; j < field[i].length; j++)
                field[i][j] = TypeCell.WALL;

        repaintNet();
    }

    /*
     * Обработчик нажатия на канву
     */
    private void clickOnCanvasHandler(MouseEvent event) {
        if (cursorType != TypeCell.NONE) {
            double x = event.getX(), y = event.getY();
            if (
                    x > 0 &&
                            (int) (x / imageSet.getSize()) < field.length &&
                            y > 0 &&
                            (int) (y / imageSet.getSize()) < field[(int) (x / imageSet.getSize())].length
                    ) {
                field[(int) (x / imageSet.getSize())][(int) (y / imageSet.getSize())] = cursorType;
                repaintNet((int) (x / imageSet.getSize()), (int) (y / imageSet.getSize()));
            }
        }
    }

    /**
     * Перерисовывает сетку по массиву field
     */
    private void repaintNet() {
        createNET();

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] != TypeCell.NONE) {
                    try {
                        repaintNet(i, j);
                    } catch (NullPointerException e) {
                        log.error("url for Image is null in [" + i + "; " + j + "]");
                    } catch (IllegalArgumentException e) {
                        log.error("url unsupported or invalid in [" + i + "; " + j + "]");
                    }
                }
            }
        }
    }

    /**
     * Перерисовывает элемент по массиву field
     */
    private void repaintNet(int i, int j) throws NullPointerException, IllegalArgumentException {
        try {
            canvas.getGraphicsContext2D().drawImage(
                    imageSet.getByName(field[i][j].toString()),
//                    new Image("picture/" + String.valueOf(edgeCell) + "/" + field[i][j].toString() + ".png"),
                    i * imageSet.getSize() + 1,
                    j * imageSet.getSize() + 1,
                    imageSet.getSize() - 2,
                    imageSet.getSize() - 2);
        } catch (NullPointerException e) {
            throw new NullPointerException();
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Расчерчивает сетку на холсте
     */
    private void createNET() {
        log.info("Рисуем сетку");

        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.RED);
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(1);
        gc.strokeLine(0, 0, 0, canvas.getHeight());
        gc.strokeLine(0, canvas.getHeight(), canvas.getWidth(), canvas.getHeight());
        gc.strokeLine(canvas.getWidth(), canvas.getHeight(), canvas.getWidth(), 0);
        gc.strokeLine(canvas.getWidth(), 0, 0, 0);

        for (int i = 0; i < canvas.getHeight() / imageSet.getSize(); i++) {
            gc.strokeLine(0, i * imageSet.getSize(), canvas.getWidth(), i * imageSet.getSize());
        }
        for (int j = 0; j < canvas.getWidth() / imageSet.getSize(); j++) {
            gc.strokeLine(j * imageSet.getSize(), 0, j * imageSet.getSize(), canvas.getHeight());
        }
    }

    /**
     * Начинаем расчёт по нажатию
     */
    @FXML
    void clickCalc() {
        log.debug("Start");
        //if (mainThread == null) {
            ca = new CA(canvasCar);
            ca.setInit(field);
            quantityCar.setDisable(true);
            int num;
            try {
                num = Integer.valueOf(quantityCar.getText());
            } catch (NumberFormatException e) {
                num = 10;
                quantityCar.setText("10");
            }
            ca.setNumCar(num);
            log.debug("quantity car == " + ca.getNumCar());
            mainThread = new Thread(ca);
            //mainThread.setDaemon(false);
            mainThread.start();
            canvas.setDisable(true);
            canvasCar.setVisible(true);
            calc.setDisable(true);
            stop.setDisable(false);
        /*} else { // Неправильно реализована логика
            ca.resume();
        }*/
    }

    @FXML
    void clickStop() {
        log.debug("Stop");
        canvas.setDisable(false);
        canvasCar.setVisible(false);
        quantityCar.setDisable(false);
        calc.setDisable(false);
        stop.setDisable(true);
        ca.pause();
    }
}
