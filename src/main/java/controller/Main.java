package controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Main extends Application {

    static final Logger log = LogManager.getLogger(Main.class);

    private Stage currStage;

    public void start(Stage primaryStage) throws Exception {

        log.info("Запуск приложения");

        currStage = primaryStage;

        try {
            if (getClass().getClassLoader().getResource("controller/Main.fxml") == null ) {
                log.error("Ресурс не найден");
                throw new Exception("Ресурс не найден");
            }
            log.debug("Ресурс найден: URI Main.fxml == " + getClass().getClassLoader().getResource(
                    "controller/Main.fxml").toString());
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(
                    "controller/Main.fxml"));
            Parent root = (Parent) fxmlLoader.load();

            FXMLCtrlMain fxmlCtrlMain = fxmlLoader.getController();
            fxmlCtrlMain.setStage(primaryStage);
            fxmlCtrlMain.setController(fxmlCtrlMain); // Запомним контроллер главного окна
            fxmlCtrlMain.init();

            Scene scene = new Scene(root);

            primaryStage.setScene(scene);
            primaryStage.setTitle("Клеточный автомат");
//            primaryStage.initModality(Modality.APPLICATION_MODAL);
            primaryStage.setResizable(false);
            primaryStage.getIcons().add(new Image("Emblemma.png"));
        } catch(Exception e) {
            e.printStackTrace();
            throw e;
        }

        primaryStage.show();
    }
}
