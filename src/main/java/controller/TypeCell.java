package controller;

//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;

public enum TypeCell {
    // Никакая
    NONE,
    // Клетка занята стенкой
    WALL,

    // Направления движения
    // U - Up, D - Down, R - Right, L - Left
    U,
    D,
    R,
    L,

    UR,
    DR,
    DL,
    UL,

    URL,
    UDR,
    DRL,
    UDL,

    UDRL;

    //private static final Logger log = LogManager.getLogger(TypeCell.class);

    /**
     * Разбивает перекрёсток на простые движения
     * @param typeCell входное направление движения
     * @return выходное нарпавление движения
     */
    public static TypeCell[] fracturedWay(TypeCell typeCell) {
        TypeCell[] typeCells = new TypeCell[typeCell.toString().length()];
        for (int i = 0; i < typeCell.toString().length(); i++) {
            typeCells[i] = TypeCell.valueOf(String.valueOf(typeCell.toString().charAt(i)));
        }
        return typeCells;
    }
}
