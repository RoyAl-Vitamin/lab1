package controller;

import javafx.scene.image.Image;

import java.util.HashSet;
import java.util.Set;

/* 460x460 */
// cell = 10x10, picture 8x8
// cell = 20x20, picture 18x18
// cell = 46x46, picture 44x44
public class WrapperImageSet {

    public static WrapperImageSet thisWrapperImageSet = null;

    public static final Set<String> apply = new HashSet<>();
    static {
        apply.add("10");
        apply.add("20");
        apply.add("46");
    }

    private Image NONE, WALL, U, D, R, L, UR, DR, DL, UL, URL, UDR, DRL, UDL, UDRL, CarH, CarV;

    private int size;

    // Singleton
    private WrapperImageSet() {}

    public static WrapperImageSet getInstance() {
        if (thisWrapperImageSet != null) {
            return thisWrapperImageSet;
        } else {
            return (thisWrapperImageSet = new WrapperImageSet());
        }
    }

    /**
     * Заполняет изображения
     */
    public void reload() throws NullPointerException, IllegalArgumentException {
        try {
            NONE = new Image("picture/" + size + "/" + "NONE" + ".png");
            WALL = new Image("picture/" + size + "/" + "WALL" + ".png");
            U = new Image("picture/" + size + "/" + "U" + ".png");
            D = new Image("picture/" + size + "/" + "D" + ".png");
            R = new Image("picture/" + size + "/" + "R" + ".png");
            L = new Image("picture/" + size + "/" + "L" + ".png");
            UR = new Image("picture/" + size + "/" + "UR" + ".png");
            DR = new Image("picture/" + size + "/" + "DR" + ".png");
            DL = new Image("picture/" + size + "/" + "DL" + ".png");
            UL = new Image("picture/" + size + "/" + "UL" + ".png");
            URL = new Image("picture/" + size + "/" + "URL" + ".png");
            UDR = new Image("picture/" + size + "/" + "UDR" + ".png");
            DRL = new Image("picture/" + size + "/" + "DRL" + ".png");
            UDL = new Image("picture/" + size + "/" + "UDL" + ".png");
            UDRL = new Image("picture/" + size + "/" + "UDRL" + ".png");
            CarH = new Image("picture/" + size + "/" + "CarH" + ".png");
            CarV = new Image("picture/" + size + "/" + "CarV" + ".png");
        } catch (NullPointerException e) { // if URL is null
            throw new NullPointerException();
        } catch (IllegalArgumentException e) { // if URL is invalid or unsupported
            throw new IllegalArgumentException();
        }
    }

    public Image getByName(String name) {
        switch (name) {
            case "NONE":
                return NONE;
            case "WALL":
                return WALL;
            case "U":
                return U;
            case "D":
                return D;
            case "R":
                return R;
            case "L":
                return L;
            case "UR":
                return UR;
            case "DR":
                return DR;
            case "DL":
                return DL;
            case "UL":
                return UL;
            case "URL":
                return URL;
            case "UDR":
                return UDR;
            case "DRL":
                return DRL;
            case "UDL":
                return UDL;
            case "UDRL":
                return UDRL;
            case "CarH":
                return CarH;
            case "CarV":
                return CarV;
        }
        return null;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
